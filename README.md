# Tandy 15 Meg Disk System Images

This project contains images of a Tandy 15 Meg Disk System created with [David Gesswein's MFM Emulator](http://www.pdp8.net/mfm/mfm.shtml).

Directory      | Contents
---------------|----------
initial_dump   | Images of disk as received from an eBay purchase
mod4p_autoboot | Images of disk after setting up LS-DOS partition to auto-boot on a Model 4P

Auto-boot instructions can be found in the HDB2/TXT file on the "Hard Disk Driver & Utilities" disk image on [this page](http://www.classiccmp.org/cpmarchives/trs80/mirrors/pilot.ucdavis.edu/davidk/trs80/software/disk_dos_page.htm#LS-DOS%20(Model%204/4P)).
