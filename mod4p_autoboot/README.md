This folder contains data dumped from a Tandy Fifteen Meg Disk System
on 2015-11-19 with David Gesswein's MFM Emulator, after installing
LS-DOS 6.03.01 on the first partition with patches for auto-booting
on the Model 4P.

Tandon TM503: 306 cylinders, 6 heads, 32 sectors of 256 bytes.

Filesystem contains six partitions, arranged one per head and spanning
all cylinders.


File          | Contents
--------------|------------------
emu_file.gz   | Emulation file from -m option, compressed with gzip.
ext_file.gz   | Extracted data file from -e option, compressed with gzip.
hdboot.imd    | ImageDisk image of boot floppy with SYSGEN parameters for this hard drive. 5.25" SSDD 40 tracks
trans_file.gz | Raw transitions file from -t option, compressed with gzip.
