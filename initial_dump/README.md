This folder contains data dumped from a Tandy Fifteen Meg Disk System
on 2015-11-18 with David Gesswein's MFM Emulator, as the drive was
received from an eBay purchase.

Tandon TM503: 306 cylinders, 6 heads, 32 sectors of 256 bytes.

Filesystem contains six partitions, arranged one per head and spanning
all cylinders.

File                 | Contents
---------------------|------------------
emu_file.gz          | Emulation file from -m option, compressed with gzip.
ext_file.gz          | Extracted data file from -e option, compressed with gzip.
mfm_read_output_file | Console output of mfm_read.
trans_file.gz        | Raw transitions file from -t option, compressed with gzip.

